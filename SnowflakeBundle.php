<?php declare(strict_types=1);

namespace KaiGrassnick\SnowflakeBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class SnowflakeBundle
 *
 * @package KaiGrassnick\SnowflakeBundle
 */
class SnowflakeBundle extends Bundle
{

}